import './App.css';
import React, { PureComponent, createRef} from 'react';
import ReactDOM from 'react-dom'
import { ScatterChart, Scatter, XAxis, YAxis, ZAxis, ResponsiveContainer, ReferenceArea, Label, Tooltip } from 'recharts';
import _ from "lodash";
import * as d3 from "d3";
import data01 from './data01';
import { Container, Slider, Grid, Typography, MenuItem, Select, TextField, Card, CardHeader, CardContent, Link } from '@material-ui/core';

class App extends PureComponent {
  static demoUrl = 'https://codesandbox.io/s/multi-bubble-chart-gb82x';
  svgRef = createRef();

  constructor(props) {
    super(props);
    this.state = {
      scalingX: d3.scaleLinear(),
      scalingY: d3.scaleLinear(),
      data: data01,
      countRange: [400, 800],
      scale: "linear",
      meetOrSlice: "slice",
      fillOpacity: 1,
      searchUrl: "",
      zoomLevel: 0
    }
  }

  renderTooltip = (props) => {
    const { active, payload } = props;
    if (active && payload && payload.length) {
      const data = payload[0] && payload[0].payload;

      return (
        <div
          style={{
            backgroundColor: '#fff',
            border: '1px solid #999',
            margin: 0,
            padding: 10,
          }}
        >
          <p>{data.name}</p>
          <p>
            <span>count: </span>
            {data.count}
          </p>
        </div>
      );
    }
    return null;
  };

  _fillOpacityChanged = (e, newValue) => {
    e.preventDefault();
    e.stopPropagation()
    this.setState({...this.state, fillOpacity: newValue});
  }

  _meetOrSliceChanged = (e) => {
    e.preventDefault();
    e.stopPropagation()
    this.setState({...this.state, meetOrSlice: e.target.value});
  }

  _scaleChanged = (e) => {
    e.preventDefault();
    e.stopPropagation()
    this.setState({...this.state, scale: e.target.value});
  }

  _searchUrl = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const baseUrl = "https://search.wellspringsoftware.net";
    const path = "/api/insights/summary.json";
    const baseQuery = {insights: "company-summary-chart"};
    const searchTerm = {q: e.target.value};
    const url = new URL(path, baseUrl);
    url.search = (new URLSearchParams({...baseQuery, ...searchTerm})).toString();
    this.setState({...this.state, searchUrl: url.toString()})
  }

  _circleElement = (e) =>  {
    let iconImageId = `${e.uid}-icon`;
    let logoUrl = e.logo_url;
    let preserveRatio = this.state.meetOrSlice === "none" ? "none" : `xMidYMid ${this.state.meetOrSlice}`;
    let path =
    <g>
      <defs>
        <pattern width="100%" height="100%" id={iconImageId} patternContentUnits="objectBoundingBox">
          <rect width="100%" height="100%" fill="white"/>
          {/* <image width="1" height="1" preserveAspectRatio="none" href={logoUrl} xlinkHref={logoUrl}/> */}
          <image width="1" height="1" href={logoUrl} xlinkHref={logoUrl} preserveAspectRatio={preserveRatio}/>
        </pattern>
      </defs>
      <path
        transform={`translate(${e.cx}, ${e.cy})`}
        d={d3.symbol("circle").size(e.size)()}
        fill={`url(#${iconImageId})`}
        stroke="black"
        strokeOpacity={this.state.fillOpacity * 0.7}
        fillOpacity={this.state.fillOpacity}
      />
    </g>
    return path;
  }

  _sliderChanged = (e, newValue) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({...this.state, countRange: newValue})
  }

  _updateData = (e) => {
    this.setState({...this.state, data: JSON.parse(e.target.value)})
  }

  componentDidMount() {
    const zoomed = () => {
    let {transform} = d3.event
    const x = transform.rescaleX(this.state.scalingX).interpolate(d3.interpolateRound);
    const y = transform.rescaleY(this.state.scalingY).interpolate(d3.interpolateRound);
    this.setState({...this.state, scalingX: x, scalingY: y})
    }

    const zoom = d3.zoom()
      .scaleExtent([0.5, 32])
      .on("zoom", zoomed);

    const svgDom = ReactDOM.findDOMNode(this.refs.svgRef).children[0];
    let svg = d3.select(svgDom)
    svg.call(zoom).call(zoom.transform, d3.zoomIdentity)
  }

  componentDidUpdate() {
    // let _d3 = d3;
    debugger;
    // d3.select(this.svgRef).append("p")
  }

  render() {
    const allData = this.state.data
    let data = _.take(allData, 25);
    data = _.sortBy(data, "count").reverse();
    const focus = _.map(data, "focus")
    const [min_focus, max_focus] = d3.extent(focus);
    const y_midpoint = d3.mean([min_focus, max_focus]);
    const focusAxisBounds = d3.extent(focus);
    // const focusAxisBounds = [min_focus - this.state.zoomLevel, max_focus + this.state.zoomLevel]

    // const impact = _.slice(data, 0, 25).map(entry => entry.impact);
    const impact = _.map(data, "impact")
    const [min_impact, max_impact] = d3.extent(impact)
    const x_midpoint = d3.mean([min_impact, max_impact]);
    // const impactAxisBounds = d3.extent(impact);
    const impactAxisBounds = [min_impact - this.state.zoomLevel / 100, max_impact + this.state.zoomLevel / 100]

    const strokeWidth = 2;
    const strokeColor = "white";
    const countDomain = d3.extent(_.map(data, "count"));
    return (
      <div>
        <Grid container>
          <Grid item xs={6}>
            <div>
              {/* <ResponsiveContainer width="100%" height={400} ref="containref"> */}
                <ScatterChart
                  ref="svgRef"
                  width={800}
                  height={400}
                  margin={{
                    top: 20,
                    right: 20,
                    bottom: 20,
                    left: 20,
                  }}
                >
                  <XAxis
                    type="number"
                    dataKey="impact"
                    domain={impactAxisBounds}
                    scale={this.state.scalingX}
                    // hide={true}
                  />
                  <YAxis
                    type="number"
                    dataKey="focus"
                    domain={focusAxisBounds}
                    allowDecimals={false}
                    scale={this.state.scalingY}
                    // hide={true}
                  />
                  <ZAxis
                    type="number"
                    dataKey="count"
                    domain={this.state.countDomain}
                    range={this.state.countRange}
                    scale={this.state.scale}
                  />
                  <ReferenceArea
                    x1={min_impact}
                    x2={x_midpoint}
                    y1={min_focus}
                    y2={y_midpoint}
                    strokeWidth={strokeWidth}
                    stroke={strokeColor}
                    strokeOpacity={1}
                    fillOpacity={0.1}
                  >
                    <Label value="Less Focus / Impact" position="insideBottomLeft" />
                  </ReferenceArea>
                  <ReferenceArea
                    x1={min_impact}
                    x2={x_midpoint}
                    y1={y_midpoint}
                    y2={max_focus}
                    stroke={strokeColor}
                    strokeWidth={strokeWidth}
                    strokeOpacity={1}
                    fillOpacity={0.1}
                  >
                    <Label value="More focus / less impact" position="insideTopLeft" />
                  </ReferenceArea>
                  <ReferenceArea
                    x1={x_midpoint}
                    x2={max_impact}
                    y1={y_midpoint}
                    y2={max_focus}
                    stroke={strokeColor}
                    strokeWidth={strokeWidth}
                    strokeOpacity={1}
                    fillOpacity={0.1}
                  >
                    <Label value="More focus / more impact" position="insideTopRight" />
                  </ReferenceArea>
                  <ReferenceArea
                    x1={x_midpoint}
                    x2={max_impact}
                    y1={min_focus}
                    y2={y_midpoint}
                    strokeWidth={strokeWidth}
                    stroke={strokeColor}
                    strokeOpacity={1}
                    fillOpacity={0.1}
                  >
                    <Label value="Less focus / more impact" position="insideBottomRight" />
                  </ReferenceArea>
                  <Tooltip content={this.renderTooltip}/>
                  <Scatter data={_.slice(data, 0, 25)} fill="#8884d8" shape={this._circleElement} ref="scatRef"/>
                </ScatterChart>
              {/* </ResponsiveContainer> */}
            </div>
          </Grid>
          <Grid container spacing={4} alignItems="center" justify="space-evenly">
            <Grid item>
              <Card>
                <CardHeader title="Bubble size range:"/>
                <CardContent>
                  <Slider
                    min={0}
                    max={6000}
                    value={this.state.countRange}
                    onChange={this._sliderChanged}
                    valueLabelDisplay="auto"
                  />
                </CardContent>
              </Card>
            </Grid>
            <Grid item>
              <Card>
                <CardHeader title="Fill Opacity"/>
                <CardContent>
                  <Slider
                    min={0}
                    max={1}
                    value={this.state.fillOpacity}
                    onChange={this._fillOpacityChanged}
                    step={0.01}
                    valueLabelDisplay="auto"
                  />
                </CardContent>
              </Card>
            </Grid>
            <Grid item>
              <Card>
                <CardHeader title="Range mapping algorithm:"/>
                <CardContent>
                  <Select
                    value={this.state.scale}
                    onChange={this._scaleChanged}
                  >
                    <MenuItem value={"linear"}>Linear</MenuItem>
                    <MenuItem value={"pow"}>Power</MenuItem>
                    <MenuItem value={"sqrt"}>Sqrt</MenuItem>
                    <MenuItem value={"log"}>Log</MenuItem>
                  </Select>
                </CardContent>
              </Card>
            </Grid>
            <Grid item>
              <Card>
                <CardContent>
                  <TextField
                    label="Data Set"
                    multiline
                    rows={4}
                    defaultValue="Data Json"
                    variant="outlined"
                    onChange={this._updateData}
                  />
                </CardContent>
              </Card>
            </Grid>
            <Grid item>
              <Card>
                <CardContent>
                  <TextField
                    label="Search"
                    variant="outlined"
                    onChange={this._searchUrl}
                  />
                  <Typography>
                    <Link href={this.state.searchUrl} target="_new">
                      Data URL
                    </Link>
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
            <Grid item>
              <Card>
                <CardHeader title="Bubble image fill"/>
                <CardContent>
                  <Select
                    value={this.state.meetOrSlice}
                    onChange={this._meetOrSliceChanged}
                  >
                    <MenuItem value={"meet"}>meet</MenuItem>
                    <MenuItem value={"slice"}>slice</MenuItem>
                    <MenuItem value={"none"}>none</MenuItem>
                  </Select>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Grid>
        <Container>
        </Container>
      </div>
    );
  }
}
export default App;
