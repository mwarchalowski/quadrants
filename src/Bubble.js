import React, {  PureComponent } from 'react';
import * as d3 from 'd3';
import PropTypes from 'prop-types'
import classNames from 'classnames/bind'

const ACTIVE_SIZE_MULTIPLIER = 1.2
const INIT_STATE = {
  active: false
}
class Bubble extends PureComponent {

  static propTypes = {
    payload: PropTypes.object,
    meetOrSlice: PropTypes.string,
    cx: PropTypes.number,
    cy: PropTypes.number,
    size: PropTypes.number,
    showLogos: PropTypes.bool,
    onMouseOver: PropTypes.func,
    onMouseOut: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {...INIT_STATE}
  }

  onMouseOver = (event) => { this.setState({active: true}); this.props.onMouseOver(event, this.props) }
  onMouseOut = (event) => { this.setState({active: false}); this.props.onMouseOut(event, this.props) }

  render() {
    const datum = this.props.payload;
    const iconImageId = `${datum.uid}-icon`;
    const logoUrl = datum.logo_url;
    const preserveRatio = this.props.meetOrSlice === 'none' ? 'none' : `xMidYMid ${this.props.meetOrSlice}`;
    const cx = this.props.cx;
    const cy = this.props.cy;
    const size = this.props.size;
    const style = {
      transform: `translate(${cx}px, ${cy}px) scale(${this.state.active ? ACTIVE_SIZE_MULTIPLIER : 1})`,
      fill: this.props.showLogos ? `url(#${iconImageId})` : null,
    }

    return(
      <g>
        <defs>
          <pattern width="100%" height="100%" id={iconImageId} patternContentUnits="objectBoundingBox">
            <rect width="100%" height="100%" fill='white'/>
            <image width="1" height="1" href={logoUrl} xlinkHref={logoUrl} preserveAspectRatio={preserveRatio}/>
          </pattern>
        </defs>
        <path
          className={classNames('bubble', {logo: this.props.showLogos, active: this.state.active})}
          id={`bubble-${datum.uid}`}
          vectorEffect="non-scaling-stroke"
          d={d3.symbol(d3.symbolCircle).size(size)()}
          style={style}
          onMouseEnter={this.onMouseOver}
          onMouseLeave={this.onMouseOut}
        />
      </g>
    )
  }
}

export default Bubble;
