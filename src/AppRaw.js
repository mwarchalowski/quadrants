import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import React, { PureComponent } from 'react';
import {Popover} from 'react-bootstrap'
import * as d3 from 'd3';
import Bubble from './Bubble'
import PropTypes from 'prop-types'
import {map, take, sortBy, isEqual} from 'underscore'

const INIT_STATE = {
  zoomTransform: null,
  bubbleSizeMultiplier: 1,
  zoomBehavior: null,
  showLogos: true
}
class QuadrantChart extends PureComponent {

  static propTypes = {
    data: PropTypes.array,
    width: PropTypes.number,
    height: PropTypes.number,
  };

  marginHorizontal = 25
  marginVertical = 25
  controlsWidth = 40
  controlsZoomFactor = 2
  scaleMapping = {
    'linear': d3.scaleLinear,
    'sqrt': d3.scaleSqrt,
    'pow': d3.scalePow,
    'log': d3.scaleLog,
    'activeBubble': null
  }

  constructor(props) {
    super(props);
    this.state = {...INIT_STATE}
  }

  zoomed = () => {
      const {transform} = d3.event;
      this.setState({
        zoomTransform: transform,
        bubbleSizeMultiplier: transform.k
      })
  }

  handleZoomReset = () => {
    d3.select(this.chart)
      .transition()
      .duration(750)
      .call(this.state.zoomBehavior.transform, d3.zoomIdentity)
  }

  handleZoomIn = () => {
    d3.select(this.chart)
      .transition()
      .duration(750)
      .call(this.state.zoomBehavior.scaleBy, this.controlsZoomFactor)
  }

  handleZoomOut = () => {
    d3.select(this.chart)
      .transition()
      .duration(750)
      .call(this.state.zoomBehavior.scaleBy, Math.pow(this.controlsZoomFactor, -1))
  }

  logoVisibilityChanged = (e) => {
    this.setState({showLogos: !this.state.showLogos})
  }

  componentDidMount() {
    const {width, height} = this.chartDimensions(this.props)
    const zoom = d3.zoom()
      .scaleExtent([1, 32])
      .extent([[0, 0], [width, height]])
      .translateExtent([[0, 0], [width, height]])
      .on('zoom', this.zoomed)
    d3.select(this.chart).call(zoom).call(zoom.transform, d3.zoomIdentity);
    this.setState({zoomBehavior: zoom})
  }

  componentDidUpdate(prevProps) {
    // TODO: Any better way of incorporating this into the lifecycle?
    const dimensions = ({width, height}) => ({width, height})
    if (!isEqual(dimensions(prevProps), dimensions(this.props))) {
      const {width, height} = this.chartDimensions(this.props)
      this.state.zoomBehavior.translateExtent([[0,0], [width, height]])
    }
  }

  activateBubble = (event, bubbleData) => {
    const activeBubble = {...bubbleData, bounds: event.target.getBoundingClientRect()};
    this.setState({activeBubble})
  }

  deactivateBubble = () => {
    this.setState({activeBubble: null})
  }

  makeTooltip = () => {
    const datum = this.state.activeBubble.payload
    const {left, bottom, width} = this.state.activeBubble.bounds
    return (
      <Popover
        id="quadrant-chart-tooltip"
        placement="bottom"
        arrowOffsetLeft={10 + width / 2}
        title={datum.name}
        style={{position: 'fixed'}}
        positionLeft={left - 10}
        positionTop={bottom + 5}
        >
        <p>Document Count: {datum.count}</p>
        <p>Focus: {datum.focus}</p>
        <p>Impact: {datum.impact}</p>
      </Popover>
    )
  }

  chartDimensions = ({width, height}) => ({
      width: width - this.controlsWidth,
      height
  })

  zoomResetButton = () => {
    return(
      <div class="zoom-controls-box zoom-reset">
        <div>
          <svg stroke='white' viewBox={'0 0 100 100'} onClick={this.handleZoomReset}>
            <g fill={'none'} strokeWidth={10}>
              <path d={'M 10 30 V 10 H 30'}/>
              <path d={'M 70 10 H 90 V 30'}/>
              <path d={'M 90 70 V 90 H 70'}/>
              <path d={'M 30 90 H 10 V 70'}/>
              <circle r={20} cx={50} cy={50}/>
            </g>
          </svg>
        </div>
      </div>
    )
  }

  logoToggleButton = () => {
    return(
      <div class="zoom-controls-box logo-toggle">
        <div onClick={this.logoVisibilityChanged}>
          <div className={this.state.showLogos ? 'active' : 'inactive'}>
            <span>Logos</span>
          </div>
          <div className={this.state.showLogos ? 'inactive' : 'active'}>
            <svg viewBox={'0 0 100 100'}>
              <g strokeWidth={2} fillOpacity={0.2}>
                <circle r={30} cx={40} cy={40}/>
                <circle r={30} cx={60} cy={60}/>
              </g>
            </svg>
          </div>
        </div>
      </div>
    )
  }

  zoomInOutButton = () => {
    return(
      <div className="zoom-controls-box zoom-in-out">
        <div>
          <i class="fa fa-plus" aria-hidden="true" onClick={this.handleZoomIn}></i>
        </div>
        <div>
          <i class="fa fa-minus" aria-hidden="true" onClick={this.handleZoomOut}></i>
        </div>
      </div>
    )
  }

  render() {
    const chartDimensions = this.chartDimensions(this.props);

    const xRange = [this.marginHorizontal, chartDimensions.width - this.marginHorizontal]
    const yRange = [this.marginVertical, chartDimensions.height - this.marginVertical].reverse()

    const allData = this.props.data
    let data = take(allData, 25);
    data = sortBy(data, 'count').reverse();

    const focus = map(allData, 'focus')
    const [minFocus, maxFocus] = d3.extent(focus);
    const yMidpoint = d3.mean([minFocus, maxFocus]);

    const impact = map(data, 'impact')
    const [minImpact, maxImpact] = d3.extent(impact)
    const xMidpoint = d3.mean([minImpact, maxImpact]);

    const countDomain = d3.extent(map(data, 'count'));

    const xScale = d3.scaleLinear().domain(d3.extent(impact)).range(xRange)
    const yScale = d3.scaleLinear().domain(d3.extent(focus)).range(yRange)

    const countRange = this.props.countRange.map(v => v / this.state.bubbleSizeMultiplier)
    const zScale = this.scaleMapping[this.props.bubbleScale]().domain(countDomain).range(countRange)

    return (
      <div className="quadrant-chart" style={{width: this.props.width, height: this.props.height}}>
        {this.state.activeBubble && this.makeTooltip()}
        <svg id="quadrants" ref={chart => this.chart = chart} {...chartDimensions}>
          <g id="bubble-wrapper" transform={this.state.zoomTransform}>
            <g id="axis">
              <line vectorEffect="non-scaling-stroke" x1={xScale(xMidpoint)} x2={xScale(xMidpoint)} y1={yScale(minFocus)} y2={yScale(maxFocus)} stroke="gray"></line>
              <line vectorEffect="non-scaling-stroke" x1={xScale(minImpact)} x2={xScale(maxImpact)} y1={yScale(yMidpoint)} y2={yScale(yMidpoint)} stroke="gray"></line>
            </g>
            {data.map((d, i) => {
              return <Bubble
              key={i}
              meetOrSlice={this.props.meetOrSlice}
              payload={d}
              cx={xScale(d.impact)}
              cy={yScale(d.focus)}
              showLogos={this.state.showLogos}
              onMouseOver={this.activateBubble}
              onMouseOut={this.deactivateBubble}
              size={zScale(d.count)}/>
            })}
          </g>
        </svg>
        <div className='emerging-organizations-chart-controls' style={{width: this.controlsWidth}}>
          {this.logoToggleButton()}
          {this.zoomResetButton()}
          {this.zoomInOutButton()}
        </div>
        <div style={{clear: 'both'}}/>
      </div>
    );
  }
}
export default QuadrantChart;
