import React, {PureComponent} from 'react'
import {Grid, Row, Col, FormGroup, ControlLabel, FormControl, Form} from 'react-bootstrap';
import { ResponsiveContainer } from 'recharts';
import QuadrantChart from './AppRaw.js';
import PropTypes from 'prop-types'

const CONTROLS_LABEL_WIDTH = 2
const CONTROLS_INPUT_WIDTH = 10
const BUBBLE_SIZE_CONTROL_RANGE = {
  min: 0,
  max: 7000
}
class EmergentOrganizationsQuadrant extends PureComponent {

  static propTypes = {
    data: PropTypes.array
  };

  constructor(props) {
    super(props)
    this.state = {
      countRange: [400, 800],
      scale: 'linear',
      meetOrSlice: 'meet',
    }
  }

  meetOrSliceChanged = (e) => {
    e.preventDefault();
    e.stopPropagation()
    this.setState({meetOrSlice: e.target.value});
  }

  scaleChanged = (e) => {
    e.preventDefault();
    e.stopPropagation()
    this.setState({scale: e.target.value});
  }

  minRangeChanged = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const newMin = parseInt(e.target.value);
    const newRange = [newMin, this.state.countRange[1]];
    this.setState({countRange: newRange})
  }

  maxRangeChanged = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const newMax = parseInt(e.target.value);
    const newRange = [this.state.countRange[0], newMax];
    this.setState({countRange: newRange})
  }

  render() {
    return(
      <Grid fluid={true}>
      <Row>
        <Col xs={12} style={{height: '80vh'}}>
          <ResponsiveContainer>
            <QuadrantChart
              data={this.props.data}
              bubbleScale={this.state.scale}
              meetOrSlice={this.state.meetOrSlice}
              countRange={this.state.countRange}/>
          </ResponsiveContainer>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form horizontal>
            <FormGroup>
              <Col sm={CONTROLS_LABEL_WIDTH}>
                <ControlLabel>Bubble range min:</ControlLabel>
              </Col>
              <Col sm={CONTROLS_INPUT_WIDTH}>
                <FormControl
                  {...BUBBLE_SIZE_CONTROL_RANGE}
                  type="range"
                  value={this.state.countRange[0]}
                  onChange={this.minRangeChanged}
                />
              </Col>
            </FormGroup>

            <FormGroup>
              <Col sm={CONTROLS_LABEL_WIDTH}>
                <ControlLabel>Bubble range max:</ControlLabel>
              </Col>
              <Col sm={CONTROLS_INPUT_WIDTH}>
                <FormControl
                  {...BUBBLE_SIZE_CONTROL_RANGE}
                  type="range"
                  value={this.state.countRange[1]}
                  onChange={this.maxRangeChanged}
                />
              </Col>
            </FormGroup>

            <FormGroup>
              <Col sm={CONTROLS_LABEL_WIDTH}>
                <ControlLabel>Bubble scaling algorithm:</ControlLabel>
              </Col>
              <Col sm={CONTROLS_INPUT_WIDTH}>
                <FormControl componentClass="select" onChange={this.scaleChanged} value={this.state.scale}>
                  <option value="linear">linear</option>
                  <option value="sqrt">sqrt</option>
                  <option value="log">log</option>
                </FormControl>
              </Col>
            </FormGroup>

            <FormGroup>
              <Col sm={CONTROLS_LABEL_WIDTH}>
                <ControlLabel>Logo Fill Strategy:</ControlLabel>
              </Col>
              <Col sm={CONTROLS_INPUT_WIDTH}>
                <FormControl componentClass="select" onChange={this.meetOrSliceChanged} value={this.state.meetOrSlice}>
                  <option value="none">none</option>
                  <option value="meet">meet</option>
                  <option value="slice">slice</option>
                </FormControl>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
    </Grid>
    )
  }
}

export default EmergentOrganizationsQuadrant;