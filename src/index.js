import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {Grid, Row, Col} from "react-bootstrap"
import ChartWrapper from "./ChartWrapper"
import data from './data01'

ReactDOM.render(
  <React.StrictMode>
    <Grid fluid>
      <Row height={400}>
        <Col>
          <ChartWrapper data={data}></ChartWrapper>
        </Col>
      </Row>
    </Grid>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
